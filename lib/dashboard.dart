import 'package:flutter/material.dart';
import 'package:module_3/pages/additems.dart';
import 'package:module_3/pages/firstpage.dart';
import 'package:module_3/pages/secondpage.dart';
import 'package:module_3/pages/userprofile.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Dashboard")),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddItem()));
        },
        child: Icon(Icons.add),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const FirstPage()));
                },
                color: Colors.blue,
                textColor: Colors.white,
                child: Text("First Page"),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SecondPage()));
                },
                color: Colors.blue,
                textColor: Colors.white,
                child: Text("Second Page"),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const UserProfile()));
                },
                color: Colors.blue,
                textColor: Colors.white,
                child: Text("User Profile"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
